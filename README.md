# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

1.brush <br> click the pencil button then you can simply choose the size and the color of the line and feel free to draw on the canvas with press the mouse down and when your mouse up, it will stop drawing until you press the mouse down again.

2.eraser <br> click the eraser button and you can choose the size of the eraser also to clean the place that you want to clean.

3.text <br> click the text button and type the word you want to write on the canvas in the text block above the text button, then click the mouse on the place that you want to place the word.

4.circle brush <br> click the circle brush button so you can simply draw a perfect circle on your canvas with press your mouse on the canvas and move to adjust the size of the circle and if you are feel everything is okay, just let your mouse up.

5.triangle brush <br> it is as same as the circle brush.

6.rectangle brush <br> same too.

7.download <br> click the download button and it will download the image of the canvas and save into your folder.

8.refresh <br> click the refresh button and it will ask that are you confirm to clean the canvas, if you are accidentally press the refresh button, just press the cancle button.

9.undo <br> click the undo button and it will go to the previous step.

10.redo <br> click the redo button after you press the undo button if you want go back to the image before you press the undo button.

### Function description

    Decribe your bonus function and how to use it.

### Gitlab page link

    web page URL, "https://108062262.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Thank you for your help in TA's hour!!!

<style>
table th{
    width: 100%;
}
</style>