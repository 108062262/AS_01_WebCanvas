var draw;
var erase;
var undo;
var redo;
var rect;
var tri;
var cir;
var typ;
var mycanvas;
var ctx;
var mode = 'none';
var startX;
var startY;
var eraser_s;

var undo_arr = [];
var redo_arr = [];
var cStep = 0;
var cPush;

function init()
{
    mycanvas=document.getElementById("myCanvas");
    ctx=mycanvas.getContext("2d");
    undo_arr.push(mycanvas.toDataURL());
}

function md(event)
{
    if(mode == 'pen')
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("brush_s").value;
        ctx.lineCap = "round";
        ctx.beginPath();
        ctx.moveTo(event.offsetX,event.offsetY);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        cStep++;
        draw = true;
        cPush = true;
    }
    else if(mode == 'eraser')
    {
        eraser_s = document.getElementById("brush_s").value;
        ctx.lineCap = "round";
        ctx.beginPath();
        ctx.moveTo(event.offsetX,event.offsetY);
        ctx.lineTo(event.offsetX, event.offsetY);
        erase = true;
        cStep++;
        cPush = true;
    }
    else if(mode == 'circle')
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("brush_s").value;
        ctx.lineCap = "round";
        ctx.beginPath();
        startX = event.offsetX;
        startY = event.offsetY;
        cir = true;
        cPush = true;
        cStep++;
    }
    else if(mode == 'triangle')
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("brush_s").value;
        ctx.lineCap = "round";
        ctx.beginPath();
        startX = event.offsetX;
        startY = event.offsetY;
        tri = true;
        cPush = true;
        cStep++;
    }
    else if(mode == 'rectangle')
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("brush_s").value;
        ctx.lineCap = "round";
        ctx.beginPath();
        startX = event.offsetX;
        startY = event.offsetY;
        rect = true;
        cPush = true;
        cStep++;
    }
    else if(mode == 'text')
    {
        cPush = true;
        cStep++;
        typ = true;
        ctx.fillStyle = document.getElementById("color").value;
		el = document.getElementById("font");
		ctx.font = document.getElementById("font_size").value+"px "+el.options[el.selectedIndex].value;
		ctx.fillText(document.getElementById("text").value, event.offsetX, event.offsetY);
    }
}

function mv(event)
{
    if(mode == 'pen')
    {
        if(draw)
        {
            ctx.lineTo(event.offsetX,event.offsetY);
            ctx.stroke();
            ctx.beginPath();
            ctx.moveTo(event.offsetX,event.offsetY);
        }
    }
    else if(mode == 'eraser')
    {
        if(erase)
        {
            ctx.lineTo(event.offsetX,event.offsetY);
            ctx.clearRect(event.offsetX,event.offsetY,eraser_s,eraser_s);
            ctx.beginPath();
            ctx.moveTo(event.offsetX,event.offsetY);
        }
    }
    else if(mode == 'circle')
    {
        if(cir)
        {
            var canvasPic = new Image();
            canvasPic.src = undo_arr[cStep-1];
            canvasPic.onload = function(){
                //refresh();
                ctx.clearRect(0, 0, mycanvas.width, mycanvas.height);
                ctx.beginPath();
                ctx.drawImage(canvasPic, 0, 0);
                ctx.arc(startX, startY, Math.sqrt(Math.pow(event.offsetX-startX, 2)+Math.pow(event.offsetY-startY, 2)), 0, 2 * Math.PI);
		    	ctx.stroke();
            }
        }
    }
    else if(mode == 'triangle')
    {
        if(tri)
        {
            var canvasPic = new Image();
            canvasPic.src = undo_arr[cStep-1];
            canvasPic.onload = function(){
                //refresh();
                ctx.clearRect(0, 0, mycanvas.width, mycanvas.height);
                ctx.beginPath();
                ctx.drawImage(canvasPic, 0, 0);
                ctx.moveTo(startX, startY);
			    ctx.lineTo(event.offsetX, event.offsetY);
			    ctx.lineTo(2*startX-event.offsetX, event.offsetY);
			    ctx.closePath();
			    ctx.stroke();
            }
        }
    }
    else if(mode == 'rectangle')
    {
        if(rect)
        {
            var canvasPic = new Image();
            canvasPic.src = undo_arr[cStep-1];
            canvasPic.onload = function(){
                //refresh();
                ctx.clearRect(0, 0, mycanvas.width, mycanvas.height);
                ctx.beginPath();
                ctx.drawImage(canvasPic, 0, 0);
                ctx.strokeRect(startX, startY, event.offsetX-startX, event.offsetY-startY);
            }
        }
    }
}

function mu()
{
    if(mode == 'pen')
    {
        if(cPush)
        {
            if(cStep<undo_arr.length)
            {
                undo_arr.length = cStep;
            }
            undo_arr.push(mycanvas.toDataURL());
        }
        draw = false;
        cPush = false;
        ctx.closePath();
    }
    else if(mode == 'eraser')
    {
        if(cPush)
        {
            if(cStep<undo_arr.length)
            {
                undo_arr.length = cStep;
            }
            undo_arr.push(mycanvas.toDataURL());
        }
        erase = false;
        cPush = false;
        ctx.closePath();
    }
    else if(mode == 'circle')
    {
        //cStep++;
        if(cPush)
        {
            if(cStep<undo_arr.length)
            {
                undo_arr.length = cStep;
            }
            undo_arr.push(mycanvas.toDataURL());
        }
        cPush = false;
        cir = false;
        ctx.stroke();
    }
    else if(mode == 'triangle')
    {
        //cStep++;
        if(cPush)
        {
            if(cStep<undo_arr.length)
            {
                undo_arr.length = cStep;
            }
            undo_arr.push(mycanvas.toDataURL());
        }
        cPush = false;
        tri = false;
        ctx.stroke();
    }
    else if(mode == 'rectangle')
    {
        //cStep++;
        if(cPush)
        {
            if(cStep<undo_arr.length)
            {
                undo_arr.length = cStep;
            }
            undo_arr.push(mycanvas.toDataURL());
        }
        cPush = false;
        rect = false;
        ctx.stroke();
    }
    else if(mode == 'text')
    {
        
        if(cPush)
        {
            if(cStep<undo_arr.length)
            {
                undo_arr.length = cStep;
            }
            undo_arr.push(mycanvas.toDataURL());
        }
        cPush = false;
        typ = false;
        document.getElementById("text").value = ' ';
    }
}

function pen()
{
    mode = 'pen';
    mycanvas.style.cursor = "url(src/mouse/pencil.png), auto";
}

function eraser()
{
    mode = 'eraser';
    mycanvas.style.cursor = "url(src/mouse/eraser.png), auto";
}

function text()
{
    mode = 'text';
    mycanvas.style.cursor = "text";
}

function circle()
{
    mode = 'circle';
    mycanvas.style.cursor = "url(src/mouse/circle.png), auto";
}

function triangle()
{
    mode = 'triangle';
    mycanvas.style.cursor = "url(src/mouse/triangle.png), auto";
}

function rectangle()
{
    mode = 'rectangle';
    mycanvas.style.cursor = "url(src/mouse/rectangle.png), auto";
}

function download()
{
    mode = 'download';
    mycanvas.style.cursor = "default";
    var dom = document.createElement("a");
    dom.href = this.mycanvas.toDataURL("image/png");
    dom.download = new Date().getTime() + ".png";
    dom.click();
}

function upload(event)
{
    mode = 'upload';
    mycanvas.style.cursor = "default";
}

function display_confirm()
{
    var c = confirm("Are you sure you want to reset?");
    mode = 'none';
    mycanvas.style.cursor = "default";
    if(c == true)
    {
        refresh();
    }
}

function refresh()
{
    ctx.clearRect(0, 0, mycanvas.width, mycanvas.height);
    undo_arr = [];
    cStep = 0;
    undo_arr.push(mycanvas.toDataURL());
}

function undo()
{
    mode = 'undo';
    mycanvas.style.cursor = "default";
    if(cStep > 0)
    {
		cStep --;
        var canvasPic = new Image();
        canvasPic.src = undo_arr[cStep];
        canvasPic.onload = function(){
            //refresh();
            ctx.clearRect(0, 0, mycanvas.width, mycanvas.height);
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}

function redo()
{
    mode = 'redo';
    mycanvas.style.cursor = "default";
    if(cStep < undo_arr.length - 1)
    {
        cStep ++;
        var canvasPic = new Image();
        canvasPic.src = undo_arr[cStep];
        canvasPic.onload = function(){
            //refresh();
            ctx.clearRect(0, 0, mycanvas.width, mycanvas.height);
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}